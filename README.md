# AvalosMonroyJavierPrac1

 Práctica a Realizar
Queremos resolver un problema de productores-consumidores. 
Buffer Selectivo
Se tiene un sistema con un número indeterminado de procesos productores y consumidores donde la operación de consumir es más lenta que la de producir, y se trata de que los consumidores no retarden la ejecución de los productores. Además, se tienen algunas características particulares: 
● Hay dos clases de datos, que llamaremos A y B. 
● Hay 2 clases de productores, los que solo producen datos de clase A y los que solo producen datos de clase B. 
● Hay 3 tipos de consumidores:
○ Los que solo consumen datos de clase A.
○ Los que solo consumen datos de clase B. 
○ Los que consumen datos de clase A o B indistintamente.
Los productores generan datos en rachas de 1 a 3, que deben ser almacenados “de golpe”.
Se tiene capacidad para almacenar NA y NB datos de clase A o B, respectivamente. 
La lectura de los datos por parte de los consumidores debe hacerse respetando el orden en que han sido depositados por los productores.

 	Proceso general:
Hemos comprendido en el enunciado que:
1.	Tenemos una capacidad máxima para cada dato (Na , Nb).
2.	Los productores producen datos de 1 a 3 y que se almacenan de golpe.
3.	Los consumidores leen los datos de manera ordenada asi dados por los productores.
4.	

Solución
Se presenta el diseño de la práctica mediante pseudocódigo donde se resolverá la ejecución necesaria para cada uno de los procesos implicados.

1.- Análisis del problema.
    Se describirán las estructuras de datos, variables compartidas y procedimientos necesarios para comprender el diseño que se realiza de la práctica.

Datos
Tipos de datos necesarios para la solución de la práctica:
    •	TDA Buffer de datos (TipoDato) 
        o	Almacena en orden FIFO elementos del TipoDato
        o	Operaciones:
            	add(TipoDato):. Inserta de forma ordenada un dato en el buffer.
            	get(TipoDato):. Coge y elimina un dato en el buffer de forma ordenada.
            	top(TipoDato): Coge pero no elimina un dato del buffer,
            	vacio(): bool.Devuelve si no hay datos en el buffer.

    •	TDA Dato
        o	tipoDato: carácter
        o	Operaciones:
            	CreaDato(): Crea un dato

    •	TDA tamRafagas
        o	tipoDato: carácter.
        o	tRafaga: entero.
        o	Operaciones:
            	CreaRafaga(). Crea las ráfagas de 1 a 3 de tamaño.

    •	TDA Productor 
        o	idPr: entero.Indentifica el productor
        o	TamRafaga:<Dato>.
        o	Operaciones:
            	producir().Produce los datos.
            	produceRafaga(). Produce las ráfagas.

    •	TDA Consumidor
        o	idC: entero.Identifica el consumidor
        o	TipoConsumidor: tipoDato. Lista de tipo de consumidores.
        o	Rafaga. Rafaga que consume
        o	Operaciones:
            	consumir().Consume los datos.

        •	TDA Main
        o	contA,contB,TMax.
        o	Buffer<Dato>. Es el buffer con los datos A y B.
        o	Buffer<tamRafagas>. Es el buffer de la ráfagas de datos
        o	sProductor(0).
        o	sConsumidor(0).
        o	sBuffer
        o	Operaciones:
            	CrearProductor():
            	CrearConsumidor():
            	EsperarFin():
            	FinalizarProductor:
            	FinalizarConsumidor():

Variables Compartidas
    Estas son las variables que comparten el Main,los productores y los consumidores:
    •	Buffer: Buffer<Dato>.  Es el buffer con los datos A y B.
    •	bRafagas: Buffer<tamRafagas>. Es el buffer de la ráfagas de datos.
    •	contA, contB, tMax. Contadores de los datos y tamaño máximo en enteros.

Semáforos
    Los semáforos para la resolución del ejercicion son los siguientes:
    •	sProductor: Representa si existe disponibilidad de producir los datos en el buffer. Se inicializa a 0.
    •	sConsumidor: Representa si existe disponibilidad de consumir los datos en el buffer. Se inicializa a 0.
    •	sBuffer: Representa si se puede acceder al buffer. Se inicializa a 1.

Procedimientos apoyo
    Para el proceso del sistema (hilo principal) necesitamos apoyarnos en los siguientes procedimientos:
    •	CrearProceso (Productores,Consumidores): Crea números aleatorios de procesos de productores y consumidores.
    •	EjecutarProceso(Productores, Consumidores): Ejecuta los procesos de productores y consumidores..
    •	EsperarProceso(Datos, Consumidores): Espera que se finalicen los datos y los consumidores.
    •	FinalizaProcesos(Productores,Consumidores): Finaliza los procesos.

Para cada proceso Dato necesitamos apoyarnos en el siguiente procedimiento:
    •	CreaDato(): Crea un dato
Para cada proceso Productor necesitamos apoyarnos en los siguiente procedimientos:
    •	producir() :Produce los datos.
    •	produceRafaga() : Produce las ráfagas entre 1 y 3 datos.
Para cada proceso Consumidor necesitamos apoyarnos en el siguiente procedimiento:
    •	consume() : Consume los datos en orden de llegada.

2.-Diseño
    Se presenta el diseño de la práctica mediante pseudocódigo donde se resolverá la ejecución necesaria para cada uno de los procesos implicados.
    Proceso Sistema (main)
        Variables locales
            Buffer<Dato>			 //Es el buffer de datos
            BufferRA<tamRafagas>		 //Es el bufer del tamaño de rafagas
            sProductor : semáforo 	//Inicializado a 0.
            sConsumidor : semáforo	 //Inicializado a 0.
            sBuffer: semáforo		//Inicializado a 1.
            contA,cont B : enteros 	//Contadores de datos A y B.
            tMax : entero			 //Tamaño máximo de los datos.
Ejecucion
	CrearProceso(Productores,Consumidores)
	EjecutarProceso(Productores,Consumidores)
    while ( EsperaRProceso(Productores, Consumidores) ) { }
    FinalizaProceso(Productores, Consumidores )
    mostrarDatos( Resultados )

Proceso Dato
    Variables locales
	    tipoDato :  carácter 		//Tipo de dato (A B)

Ejecucion
	Dato() {
		tipoDato = tipo
	}
Proceso Productor
    Variables locales
	    idPr : carácter 	
	    TamRafaga<Dato>
	    tipoDato : tipoDato
Ejecucion_proceso{
	producir()
	produceRafagas()
}
producir(){
        for i = 1 to aletorio_rafaga() do
            TamRafaga= produceRafagas(tipoDato);
            sBuffer.wait
            tipo: carácter
            cont : entero
            if (tipo ==’A’)
                cont=contA 
            else
                cont=contB;
            tam: entero
            tam=TamRafaga.size
            while(cont+TamRafaga.size>tMax)	
            sBuffer.signal		
                sProductor.wait		
                sBuffer.wait
                if (tipo ==’A’)
                    cont=contA 
                else
                    cont=contB;
            for j = 1 to tam do 
                Buffer.add(TamRafagas.get(i))
            rec= tamRafaga(tipo, tam)
            BufferRA.add(rec)
            if (tipo ==’A’)
                cont=contA +tam  
            else
                cont=contB + tam ;
            sBuffer.signal;
            sConsumidor.signal
    produceRafaga(tipo ): Buffer 	// genera la ráfaga y guarda en el buffer interno
        for j -> 1 to numero_aleatorio_datos(3)+1 do
    dato: Dato
    espera_tiempo_aleatorio(MIN_TIEMPO _PRODUCIR) + VARIACION_TIEMPO
    dato= Dato(tipo)		//creo un dato de un tipo dado
    bufferDatos.add(dato) //guardo localmente los datos de la ráfaga 
    devolver Buffer




Proceso Consumidor
Variables locales
	idC : entero
	tipoConsumidor : tipoDato
	Rafaga : tamRafagas
Ejecucion_proceso (): lista < Dato >
	listaResultados: lista < Dato >
    while no(interrumpido)	 //mientras que no sea interrumpido por hilo principal
    consume () 		// saca y procesa del buffer una ráfaga 
    devolver listaResultados	//sale lista de Datos consumidos por el consumidor



    consume ()
    while bRafagas.empty() 
    sConsumidor.wait	
        Rafaga=BufferRA.top()
    while tipoConsumidor == null && tipoConsumidor == Rafaga.tipoConsumidor
    sConsumidor.wait		
    Rafaga=BufferRA.top() 	
    sBuffer.wait		
    tamR: entero			
    tamR = Rafaga.tam
    for i -> 1 to tamR do
    listaResultados.add(buffer.get())    

    if (proxReg.tipo == 'A') 		
    contA = contA – tamR		
    else
    contB = contB - tamR
    Rafaga = BufferRA.get()		
    sBuffer.signal	
    sConsumidor.signal 	
    mutexMain.signal		
    sProductor.signal	
    espera_tiempo_Aleatorio (MIN_TIEMPO _CONSUMIR) + VARIACION_TIEMPO * tamR					
    }
