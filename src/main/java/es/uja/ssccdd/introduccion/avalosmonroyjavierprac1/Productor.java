/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.introduccion.avalosmonroyjavierprac1;

import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.MAX;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.MAX_RACHA;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.MIN_RACHA;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.MIN_TIEMPO_PRODUCIR;
import es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.TipoElemento;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.VARIACION_TIEMPO_PRODUCIR;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class Productor implements Runnable{
    TipoElemento tipoelemento;
    ArrayList<Elemento> Buffer;
    int elementos[];
    int numConsumidores[];  
    int numProductores[][];
    Semaphore exmBuffer;
    Semaphore exmElm;
    Semaphore exmConsumidores;
    Semaphore sincProductor[][];
    Semaphore sincConsumidor[];
                                  
    
    public Productor( TipoElemento tipoelemento, ArrayList<Elemento> BufferSelectivo, int elementos[], 
                        int numConsumidores[], int numProductores[][], Semaphore exmBuffer, Semaphore exmElm, 
                        Semaphore exmConsumidores, Semaphore sincProductor[][], Semaphore sincConsumidor[] ){
        this.Buffer = BufferSelectivo;
        this.elementos = elementos;
        this.numConsumidores = numConsumidores;
        this.numProductores = numProductores;
        this.exmBuffer = exmBuffer;
        this.exmElm = exmElm;
        this.exmConsumidores = exmConsumidores;
        this.sincProductor = sincProductor;
        this.sincConsumidor = sincConsumidor;               
        
        this.tipoelemento = tipoelemento;
    }
    
    @Override
    public void run() {
        int racha= aleatorio.nextInt(MAX_RACHA)+1;
        
        System.out.println(" PRODUCTOR produciendo racha de "+racha+" elem. de tipo: "+ tipoelemento.name() );
        
        int tiempoproducir = (MIN_TIEMPO_PRODUCIR + aleatorio.nextInt(VARIACION_TIEMPO_PRODUCIR))*racha;
        try {
            TimeUnit.SECONDS.sleep(tiempoproducir);
        } catch (InterruptedException ex) {
           System.out.println("Productor cancelado");
           return;
        }
        
        try {
            // Nos aseguramos que podemos almacenar la racha
            // o se bloquea el productor en el tamaño de la racha
            exmElm.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(Productor.class.getName()).log(Level.SEVERE, null, ex);
        }
        if ( elementos[tipoelemento.ordinal()] + racha <= MAX[tipoelemento.ordinal()] ){
            elementos[tipoelemento.ordinal()] += racha;
            exmElm.release();
        }else{
            numProductores[tipoelemento.ordinal()][racha-1]++;  
            exmElm.release();
            try {
                sincProductor[tipoelemento.ordinal()][racha-1].acquire();
            } catch (InterruptedException ex) {
               System.out.println("Productor cancelado");
               return;
            }
        }
    
        // Se almacena la racha
        boolean finRacha;        
        
        try {
            exmBuffer.acquire();
        } catch (InterruptedException ex) {
               System.out.println("Productor cancelado");
               return;
        }
        
        for (int i=1; i<=racha; i++){
            if (i == racha)
                finRacha = true;
            else
                finRacha = false;
            Elemento elemento= new Elemento(tipoelemento,finRacha);
            Buffer.add(elemento);
        }
        exmBuffer.release();
        System.out.println(" PRODUCTOR añade al Buffer racha de "+racha+" elem. de tipo: "+ tipoelemento.name());
        
        try {
            // Liberamos a un consumidor
            exmConsumidores.acquire();
        } catch (InterruptedException ex) {
               System.out.println("Productor cancelado");
               return;
        }
        if( numConsumidores[TipoElemento.AoB.ordinal()] > 0 ){
            numConsumidores[TipoElemento.AoB.ordinal()]--;
            sincConsumidor[TipoElemento.AoB.ordinal()].release();
        }else{
            if( numConsumidores[tipoelemento.ordinal()] > 0 ){            
                numConsumidores[tipoelemento.ordinal()]--;
                sincConsumidor[tipoelemento.ordinal()].release();
            }
        }
        exmConsumidores.release();
        
        // Comprobamos si tenemos que liberar un productor
        // porque hay sitio en el buffer para producir
        boolean liberado;
        int tamanoRacha;
        try {
            exmElm.acquire();
        } catch (InterruptedException ex) {
               System.out.println("Productor cancelado");
               return;
        }
        liberado = false;
        tamanoRacha = MAX_RACHA;
        while( tamanoRacha >= MIN_RACHA && !liberado ){
            if( elementos[tipoelemento.ordinal()] + tamanoRacha <= MAX[tipoelemento.ordinal()] && //Comprueba si hay hueco
                    numProductores[tipoelemento.ordinal()][tamanoRacha-1]>0 ){   //Comprueba si cabe la racha
                elementos[tipoelemento.ordinal()] += tamanoRacha;
                numProductores[tipoelemento.ordinal()][tamanoRacha-1]--;   
                liberado = true;
                sincProductor[tipoelemento.ordinal()][tamanoRacha-1].release();
            }
            tamanoRacha--;
        }
        exmElm.release();     
    }
}
