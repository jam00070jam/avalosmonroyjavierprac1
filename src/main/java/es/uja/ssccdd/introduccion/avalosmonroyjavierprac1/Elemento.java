/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.introduccion.avalosmonroyjavierprac1;

import es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.TipoElemento;

/**
 *
 * @author Usuario
 */
public class Elemento {
     TipoElemento tipoelemento;
    boolean finRacha;
    
    public Elemento(TipoElemento tipoE, boolean tipoR){
        this.tipoelemento = tipoE;
        this.finRacha = tipoR;
    }
    
    TipoElemento getTipoElemento(){
        return this.tipoelemento;
    }
    
    Boolean finRacha(){
        return this.finRacha;
    }
    
    @Override
    public String toString(){
        return this.tipoelemento.name();
    }
    
    @Override
    public boolean equals(Object obj) {
        final Elemento ele = (Elemento)obj;
        return this.tipoelemento.equals(ele.tipoelemento);
    }
}
