/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.introduccion.avalosmonroyjavierprac1;

import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.MAX;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.MAX_RACHA;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.MIN_RACHA;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.MIN_TIEMPO_CONSUMIR;
import es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.TipoElemento;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.VARIACION_TIEMPO_CONSUMIR;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class Consumidor implements Runnable {
     TipoElemento tipoelemento;
    
    ArrayList<Elemento> Buffer;
    int elementos[];
    int numConsumidores[];
    int numProductores[][];    
    Semaphore exmBuffer;
    Semaphore exmElm;
    Semaphore exmConsumidores;
    Semaphore sincProductor[][];
    Semaphore sincConsumidor[];
                                  
    //Constructor
    public Consumidor( TipoElemento tipoelemento, ArrayList<Elemento> BufferSelectivo, int elementos[], 
                        int numConsumidores[], int numProductores[][], Semaphore exmBuffer, Semaphore exmElm, 
                        Semaphore exmConsumidores, Semaphore sincProductor[][], Semaphore sincConsumidor[] ){
        this.Buffer = BufferSelectivo;
        this.elementos = elementos;
        this.numConsumidores = numConsumidores;
        this.numProductores = numProductores;
        this.exmBuffer = exmBuffer;
        this.exmElm = exmElm;
        this.exmConsumidores = exmConsumidores;
        this.sincProductor = sincProductor;
        this.sincConsumidor = sincConsumidor;               
        
        this.tipoelemento = tipoelemento;
    }
    
   @Override
    public void run(){//Se ejecuta el consumidor hasta que sea cancelado
       try {
           while (true){   
                ejecucion();
           }
       } catch (InterruptedException ex) {
            System.out.println("Consumidor cancelado");
       }
    }
    
    private void ejecucion() throws InterruptedException {
       int racha;
        
       try {
           // Sumamos el consumidor para saber los que están
           // esperamos hasta que haya un elemento de su tipo
           exmConsumidores.acquire();
       } catch (InterruptedException ex) {
           Logger.getLogger(Consumidor.class.getName()).log(Level.SEVERE, null, ex);
       }
       numConsumidores[tipoelemento.ordinal()]++;
       exmConsumidores.release();
       
       System.out.println(" CONSUMIDOR tipo: "+tipoelemento.name()+", en espera de racha" );
       try {           
                  
           sincConsumidor[tipoelemento.ordinal()].acquire();
          
       } catch (InterruptedException ex) {
           throw ex;
       }    
        
        try {
            // Se consume la racha del tipo elemento
            exmBuffer.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(Consumidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        racha = 0;
        Elemento elemento= null;
        do{
            if( tipoelemento.ordinal() == TipoElemento.AoB.ordinal() ){
                elemento = Buffer.get(0);
                Buffer.remove(0);
            }else{ 
                Elemento ele= new Elemento(tipoelemento,true);  
                int pos = Buffer.indexOf(ele);//Se coge el indice del elemento del buffer
                elemento= Buffer.get(pos);
                Buffer.remove(pos);
            }
            racha++;
        }while(!elemento.finRacha());        
        exmBuffer.release();
     
        System.out.println(" CONSUMIDOR tipo: "+tipoelemento.name()+", consumiendo racha de "+racha+" de tipo: "+ elemento.getTipoElemento().name());
        
        int tiempoconsumir;
        tiempoconsumir = (MIN_TIEMPO_CONSUMIR + aleatorio.nextInt(VARIACION_TIEMPO_CONSUMIR))*racha;
        try {
            TimeUnit.SECONDS.sleep(tiempoconsumir);
        } catch (InterruptedException ex) {

            throw ex;
        }
    
        try {
            exmElm.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(Consumidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int tipo = elemento.getTipoElemento().ordinal();
        elementos[tipo] -= racha;
        boolean liberado = false;
        int tamanoRacha = MAX_RACHA;
        while ( tamanoRacha >= MIN_RACHA && !liberado ){
            if ( elementos[tipo] + tamanoRacha <= MAX[tipo]  && //comprueba si cabe la racha 
                    numProductores[tipo][tamanoRacha-1]>0 ){    //comprueba que se pueda producir la racha 
                elementos[tipo] += tamanoRacha;
                numProductores[tipo][tamanoRacha-1]--;        
                liberado = true;
                sincProductor[tipo][tamanoRacha-1].release();
            }
            tamanoRacha--;
        }
        
        exmElm.release();
                
    }
}
