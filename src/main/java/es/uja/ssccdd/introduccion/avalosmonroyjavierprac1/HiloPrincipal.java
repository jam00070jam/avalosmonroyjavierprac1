/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.introduccion.avalosmonroyjavierprac1;

import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.MAX_RACHA;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.NUM_CONSUMIDORES;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.NUM_PRODUCTORES;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.NUM_TIPO_ELEM;
import es.uja.ssccdd.introduccion.avalosmonroyjavierprac1.Constantes.TipoElemento;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


/**
 *
 * @author Usuario
 */
public class HiloPrincipal {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     * @throws java.util.concurrent.ExecutionException
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        // TODO code application logic here
        
        ArrayList<Elemento> BufferSelectivo = new ArrayList<>();
        int elementos[]={0,0};
        int numConsumidores[]={0,0,0};
        int numProductores[][]={{0,0,0},{0,0,0}};
        
        Semaphore exmBuffer = new Semaphore(1);
        Semaphore exmElm = new Semaphore(1);
        Semaphore exmConsumidores = new Semaphore(1);
        Semaphore sincProductor[][]= new Semaphore[NUM_TIPO_ELEM][MAX_RACHA];
        for (int i=0; i<NUM_TIPO_ELEM; i++)
            for (int j=0; j<MAX_RACHA; j++)
                sincProductor[i][j]= new Semaphore(0);
        Semaphore sincConsumidor[];
        sincConsumidor = new Semaphore[NUM_TIPO_ELEM+1];
        for (int i=0; i<NUM_TIPO_ELEM+1; i++)
            sincConsumidor[i]= new Semaphore(0);
        
               
        System.out.println("** Hilo(PRINCIPAL): Ha iniciado la ejecucion\n");     
        
        ExecutorService executor = Executors.newFixedThreadPool(NUM_PRODUCTORES+NUM_CONSUMIDORES);
        List<Future<?>> procesos= new ArrayList<>();
        
        for (int i=0; i<NUM_CONSUMIDORES; i++) {
            TipoElemento tipoelemento= TipoElemento.getTipoElementoConsumidor();
            Consumidor consumidor = new Consumidor(tipoelemento, BufferSelectivo, elementos, numConsumidores, numProductores,
                                                exmBuffer, exmElm, exmConsumidores, sincProductor, sincConsumidor);
            procesos.add(executor.submit(consumidor));
        }
        for (int i=0; i<NUM_PRODUCTORES; i++) {
            TipoElemento tipoelemento= TipoElemento.getTipoElementoProductor();
            Productor productor = new Productor(tipoelemento, BufferSelectivo, elementos, numConsumidores, numProductores,
                                                exmBuffer, exmElm, exmConsumidores, sincProductor, sincConsumidor);

            procesos.add(executor.submit(productor));
        }

        //No recibe mas tareas
        executor.shutdown();
       
                
        int c=0;
        boolean cancelados=false;
        //Se espera a que los hilos finalizen     
        while (!executor.isTerminated()){
            System.out.println(".....");
            c+=2;
            TimeUnit.SECONDS.sleep(2);
            if (c>(NUM_PRODUCTORES+NUM_CONSUMIDORES*2)+10){
                for (Future<?> proceso: procesos){
                    if (!proceso.isDone()){
                        proceso.cancel(true);
                        cancelados=true;                        
                    }
                }
               
            }
        }
         if (cancelados)
            System.out.println("Cancelados los procesos pdtes de finalización/bloqueados.");
               
        
        System.out.println("\n** Hilo(PRINCIPAL): Todos los hilos han terminado\n");

        
        System.out.println("** Hilo(PRINCIPAL): Ha finalizado la ejecucion");
    }
}
    

