/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.introduccion.avalosmonroyjavierprac1;

import java.util.Random;

/**
 *
 * @author Usuario
 */
public class Constantes {
    // Genera numero aleatorio
    public static final Random aleatorio = new Random();
   
    
    // Tipo de elemento
    public enum TipoElemento {
        A(0), B(1), AoB(2);

        private final int valor;

        private TipoElemento(int valor) {
            this.valor = valor;
        }
        
        public static TipoElemento getTipoElementoProductor() {
            int valorAleatorio = aleatorio.nextInt(NUM_TIPO_ELEM);
            return TipoElemento.values()[valorAleatorio];
        }
        
        public static TipoElemento getTipoElementoConsumidor() {
            int valorAleatorio = aleatorio.nextInt(NUM_TIPO_ELEM+1);
            return TipoElemento.values()[valorAleatorio];
        }
        
    }
    
    // Constantes de la clase
    public static final int MAX[]= {8,8}; 
    public static final int NUM_TIPO_ELEM = 2;
    
    public static final int MIN_RACHA = 1;
    public static final int MAX_RACHA = 3;

    public static final int MIN_TIEMPO_PRODUCIR = 2;
    public static final int VARIACION_TIEMPO_PRODUCIR = 2;
    
    public static final int MIN_TIEMPO_CONSUMIR = 3;
    public static final int VARIACION_TIEMPO_CONSUMIR = 5;
    
    public static final int NUM_PRODUCTORES = 6;
    public static final int NUM_CONSUMIDORES = 6;
}
